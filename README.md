PASTMUS: mapping functional elements at single amino acid resolution in human cells

Xinyi Zhang, Di Yue, Yinan Wang, Yuexin Zhou, Ying Liu, Yeting Qiu, Feng Tian, Ying Yu, Zhuo Zhou & Wensheng Wei

Identification of functional elements for a protein of interest is important for achieving a mechanistic understanding. However, it remains cumbersome to assess each and every amino acid of a given protein in relevance to its functional significance. Here, we report a strategy, PArsing fragmented DNA Sequences from CRISPR Tiling Mutagenesis Screening (PASTMUS), which provides a streamlined workflow and a bioinformatics pipeline to identify critical amino acids of proteins in their native biological contexts. Using this approach, we map six proteins - three bacterial toxin receptors and three cancer drug targets, and acquire their corresponding functional maps at amino acid resolution.

The demo data of PASTMUS computational pipeline is provided at https://figshare.com/articles/PASTMUS_mapping_functional_elements_at_single_amino_acid_resolution_in_human_cells/10435370.


