#!/bin/sh

bam_proc(){
	inputPath=$1
	outputPath=$2
	bam_file=$3

	## sort bam
	samtools sort ${outputPath}${bam_file}.out.bam > ${outputPath}${bam_file}.sort
	## exact mutation reads from bam file 
	samtools view -h ${outputPath}${bam_file}.sort | grep -v 'NM:i:0' > ${outputPath}${bam_file}.ext.sam
	samtools view -b ${outputPath}${bam_file}.ext.sam > ${outputPath}${bam_file}.ext.bam
	samtools sort ${outputPath}${bam_file}.ext.bam -o ${outputPath}${bam_file}.ext.sort
	samtools index ${outputPath}${bam_file}.ext.sort
	## calculate sequencing depth 
	samtools depth -d 0 ${outputPath}${bam_file}.sort > ${outputPath}${bam_file}.coverage
	samtools depth -d 0 ${outputPath}${bam_file}.ext.sort > ${outputPath}${bam_file}.ext.coverage
	samtools stats ${outputPath}${bam_file}.sort > ${outputPath}${bam_file}.sort.map.stats
	samtools stats ${outputPath}${bam_file}.ext.sort > ${outputPath}${bam_file}.ext.map.stats
}

map_reads(){

	inputPath=$1
	outputPath=$2
	gene=$3
	R1_prefix=$4
	R2_prefix=$5
	suffix=$6
	shift 6
	readsIndex=("$@")
	
	echo ${readsIndex[@]}
	
	for i in ${readsIndex[@]};
	do
	bowtie2 -p 16 -x ${gene_index} -1 ${inputPath}${R1_prefix}${i}${suffix} -2 ${inputPath}${R2_prefix}${i}${suffix} -S ${outputPath}${gene}_${i}.sam
	
	samtools view -bF 4 ${outputPath}${gene}_${i}.sam > ${outputPath}mapped_${gene}_${i}.sam 
	done
	
	samtools cat -o ${outputPath}${gene}.out.bam ${outputPath}mapped_${gene}_*

	bam_file=${gene}
	bam_proc ${inputPath} ${outputPath} ${bam_file}
	rm -f ${outputPath}*sam


}

bowtiePath="/bowtie2-2.3.4-linux-x86_64/"
samtoolPath="/samtools-1.6/"

gene_index="PSMB5"
cd ${bowtiePath}
bowtie2-build psmb5_NM_002797_hg19_mrna.fa ${gene_index}


inputPath="~/demo_data/PSMB5_ref_rep1/"
outputPath="~/demo_data/bam_PSMB5_ref_rep1/"
mkdir $outputPath

R1="filter_PSMB5_R1_D14_S1_L005_R1_001.fastq"
R2="filter_PSMB5_R1_D14_S1_L005_R2_001.fastq"
suffix=".fq.gz"

split_label=("ac" "ad")
gene="PSMB5_ref_rep1"

map_reads ${inputPath} ${outputPath} ${gene} ${R1} ${R2} ${suffix} ${split_label[*]}



inputPath="~/demo_data/PSMB5_ref_rep2/"
outputPath="~/demo_data/bam_PSMB5_ref_rep2/"
mkdir $outputPath

R1="filter_PSMB5_R2_D14_S2_L003_R1_001.fastq"
R2="filter_PSMB5_R2_D14_S2_L003_R2_001.fastq"
suffix=".fq.gz"

split_label=("ac" "ad")
gene="PSMB5_ref_rep2"
map_reads ${inputPath} ${outputPath} ${gene} ${R1} ${R2} ${suffix} ${split_label[*]}



inputPath="~/demo_data/PSMB5_exp_rep1/"
outputPath="~/demo_data/bam_PSMB5_exp_rep1/"
mkdir $outputPath

R1="filter_PSMB5_R1_S2_L006_R1_001.fastq"
R2="filter_PSMB5_R1_S2_L006_R2_001.fastq"
suffix=".fq.gz"

split_label=("ac" "ad")
gene="PSMB5_exp_rep1"
map_reads ${inputPath} ${outputPath} ${gene} ${R1} ${R2} ${suffix} ${split_label[*]}


inputPath="~/demo_data/PSMB5_exp_rep2/"
outputPath="~/demo_data/bam_PSMB5_exp_rep2/"
mkdir $outputPath

R1="filter_PSMB5_R2_S3_L006_R1_001.fastq"
R2="filter_PSMB5_R2_S3_L006_R2_001.fastq"
suffix=".fq.gz"

split_label=("ac" "ad")
gene="PSMB5_exp_rep2"
map_reads ${inputPath} ${outputPath} ${gene} ${R1} ${R2} ${suffix} ${split_label[*]}





